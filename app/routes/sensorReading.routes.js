module.exports = app => {
    const sensorReadings = require("../controllers/sensorReading.controller.js");
  
    // Retrieve all sensorReadings
    app.get("/sensorReadings", sensorReadings.findAll);
  
  }; 