const SensorReading = require("../models/sensorReading.model.js");

// Retrieve all Customers from the database.
exports.findAll = (req, res) => {
  SensorReading.getAll((err, data) => {
      if(err)
        res.status(500).send({
            message:
               err.message || "Some error occurred while retrieving sensor readings."
        });
       else {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.send(data); 
       } 
  })
};