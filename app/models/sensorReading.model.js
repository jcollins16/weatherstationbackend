const sql = require('./db.js');

const SensorReading = function(sensorReading) {
    this.sensor = sensorReading.sensor;
    this.temperature = sensorReading.temperature;
    this.humidity = sensorReading.humidity;
    this.timestamp = sensorReading.timestamp; 
}

SensorReading.getAll = result => {
    sql.query("SELECT * FROM temp", (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
    
        console.log("temp table: ", res);
        result(null, res);
      });
}

module.exports = SensorReading;